/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/07 15:22:41 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/13 19:11:56 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		free_array(char **array)
{
	int		i;

	i = 0;
	while (array[i])
	{
		free(array[i]);
		i++;
	}
}

void		get_player(char *s, t_fill *f)
{
	if (!(f->p1))
	{
		if (s[10] == '1')
		{
			f->p1 = 'O';
			f->p2 = 'X';
		}
		else
		{
			f->p1 = 'X';
			f->p2 = 'O';
		}
	}
	free(s);
	s = NULL;
}

void		get_board(char **s, t_fill *f)
{
	int		i;
	char	**dimensions;

	i = 0;
	dimensions = ft_strsplit(*s, ' ');
	f->board_height = ft_atoi(dimensions[1]);
	f->board_width = ft_atoi(dimensions[2]);
	free_array(dimensions);
	free(*s);
	get_next_line(0, s);
	free(*s);
	f->board = (char **)malloc(sizeof(char *) * f->board_height);
	while (i < f->board_height)
	{
		get_next_line(0, s);
		f->board[i] = ft_strdup(*s + 4);
		free(*s);
		i++;
	}
}

void		get_piece(char **s, t_fill *f)
{
	int		i;
	char	**dimensions;

	i = 0;
	dimensions = ft_strsplit(*s, ' ');
	f->piece_height = ft_atoi(dimensions[1]);
	f->piece_width = ft_atoi(dimensions[2]);
	free_array(dimensions);
	free(*s);
	f->piece = (char **)malloc(sizeof(char *) * f->piece_height);
	while (i < f->piece_height)
	{
		get_next_line(0, s);
		f->piece[i] = ft_strdup(*s);
		free(*s);
		i++;
	}
}
