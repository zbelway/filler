
NAME = filler

SRC = main.c \
	  input.c \
	  algo.c \
	  directional_attack.c \
	  quadrant.c \
	  distance_formula.c

OBJ = $(subst .c,.o,$(SRC))

FLAGS = -Wall -Werror -Wextra

CC = gcc

RM = rm -rf

LIBS = -L./libft -lft -L./ft_printf -lftprintf

INCLUDES = -I./libft/includes -I./ft_printf/includes

all: $(NAME)

$(NAME): $(OBJ)
	make -C ./ft_printf
	make -C ./libft
	$(CC) $(FLAGS) $(LIBS) $(INCLUDES) -o $(NAME) $(OBJ)

%.o: %.c
	$(CC) $(FLAGS) $(INCLUDES) -c -o$@ $^

clean:
	make clean -C ./ft_printf
	make clean -C ./libft
	$(RM) $(OBJ)

fclean: clean
	make fclean -C ./ft_printf
	make fclean -C ./libft
	$(RM) $(NAME)

re: fclean all clean

.PHONY: re fclean clean all
