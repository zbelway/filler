/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   distance_formula.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/14 21:01:09 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/14 21:31:50 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

double		abs_val(double d)
{
	if (d >= 0)
		return (d);
	else
		return (d * -1);
}

double		square_root(double x, double high, double mid, double low)
{
	double	last_mid;
	double	sqrt;

	sqrt = -1;
	while (abs_val(x - sqrt) >= .0001)
	{
		last_mid = mid;
		mid = (high + low) / 2;
		sqrt = mid * mid;
		if (sqrt > x)
			high = mid;
		else
			low = mid;
	}
	return (mid);
}

double		distance(int x, int y)
{
	int		square;

	square = (x * x) + (y * y);
	if (square == 0 || square == 1)
		return (square);
	return (square_root(square, square, square, 0));
}
