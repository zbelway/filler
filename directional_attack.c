/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   directional_attack.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 19:11:25 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/14 23:11:47 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		change_piece(t_fill *f, int quadrant)
{
	if (quadrant > 2)
	{
		f->piece_start_i = 0;
		f->piece_end_i = f->piece_height;
		f->piece_iter_i = 1;
	}
	if (quadrant == 2 || quadrant == 3)
	{
		f->piece_start_j = 0;
		f->piece_end_j = f->piece_width;
		f->piece_iter_j = 1;
	}
}

void		change_side(t_fill *f, int quadrant)
{
	if (quadrant > 2)
	{
		f->board_start_i = f->board_height - 1;
		f->board_end_i = -1;
		f->board_iter_i = -1;
	}
	if (quadrant == 2 | quadrant == 3)
	{
		f->board_start_j = f->board_width - 1;
		f->board_end_j = -1;
		f->board_iter_j = -1;
	}
}

void		initialize_iteration(t_fill *f)
{
	f->board_start_i = 0;
	f->board_start_j = 0;
	f->board_end_i = f->board_height;
	f->board_end_j = f->board_width;
	f->board_iter_i = 1;
	f->board_iter_j = 1;
	f->piece_start_i = f->piece_height - 1;
	f->piece_end_i = -1;
	f->piece_iter_i = -1;
	f->piece_start_j = f->piece_width - 1;
	f->piece_end_j = -1;
	f->piece_iter_j = -1;
}

void		get_iteration(t_fill *f)
{
	int		quadrant;

	quadrant = 1;
	initialize_iteration(f);
	quadrant = get_quadrant(f, f->p1_placed);
	initialize_iteration(f);
	change_side(f, quadrant);
	quadrant = get_quadrant(f, 0);
	change_piece(f, quadrant);
}

void		get_directionality(t_fill *f)
{
	f->bi = -1;
	f->p1_placed = 0;
	f->p2_placed = 0;
	f->p2x = (int *)malloc(sizeof(int) * (f->board_width * f->board_height));
	f->p2y = (int *)malloc(sizeof(int) * (f->board_width * f->board_height));
	while (++f->bi < f->board_height)
	{
		f->bj = -1;
		while (++f->bj < f->board_width)
		{
			if (f->board[f->bi][f->bj] == f->p1
					|| f->board[f->bi][f->bj] == f->p1 + 32)
				f->p1_placed++;
			if (f->board[f->bi][f->bj] == f->p2
					|| f->board[f->bi][f->bj] == f->p2 + 32)
			{
				f->p2x[f->p2_placed] = f->bj;
				f->p2y[f->p2_placed++] = f->bi;
			}
		}
	}
	get_iteration(f);
}
