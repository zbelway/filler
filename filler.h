/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/07 14:19:04 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/15 15:50:42 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H
# include <ft_printf.h>
# include <libft.h>

typedef struct		s_fill
{
	char			p1;
	char			p2;
	char			**board;
	char			**piece;
	int				*p2x;
	int				*p2y;
	int				p1_placed;
	int				p2_placed;
	int				board_width;
	int				board_height;
	int				piece_width;
	int				piece_height;
	int				px;
	int				py;
	int				bi;
	int				bj;
	int				pi;
	int				pj;
	int				touch;
	int				board_start_i;
	int				board_start_j;
	int				board_end_i;
	int				board_end_j;
	int				board_iter_i;
	int				board_iter_j;
	int				piece_start_i;
	int				piece_start_j;
	int				piece_end_i;
	int				piece_end_j;
	int				piece_iter_i;
	int				piece_iter_j;
}					t_fill;

void				get_player(char *s, t_fill *f);
void				get_board(char **s, t_fill *f);
void				get_piece(char **s, t_fill *f);
void				scan_board(t_fill *f);
void				print_board(t_fill *f);
void				get_directionality(t_fill *f);
void				change_side(t_fill *f, int quadrant);
void				print_board(t_fill *f);
int					get_quadrant(t_fill *f, int placed);
double				abs_val(double d);
double				distance(int x, int y);

#endif
