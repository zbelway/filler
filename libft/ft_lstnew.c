/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_listnew.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/15 17:37:46 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/27 17:05:31 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

t_list		*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*list;

	list = (t_list *)ft_memalloc(sizeof(t_list));
	if (!list)
		return (list);
	if (content)
	{
		if ((list->content = ft_memalloc(content_size)) != NULL)
		{
			list->content = ft_memcpy(list->content, content, content_size);
			list->content_size = content_size;
		}
		else
			list->content_size = 0;
	}
	else
	{
		list->content = (void *)content;
		list->content_size = 0;
	}
	list->next = NULL;
	return (list);
}
