/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 19:17:34 by zbelway           #+#    #+#             */
/*   Updated: 2016/01/23 15:12:07 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

void		*ft_memmove(void *s1, const void *s2, size_t n)
{
	char	*tmp;
	size_t	i;

	i = 0;
	tmp = s1;
	if (s1 < s2)
		ft_memcpy(s1, s2, n);
	else
	{
		s1 = (char *)s1 + n - 1;
		s2 = (char *)s2 + n - 1;
		while (i < n)
		{
			*(char *)s1-- = *(char *)s2--;
			i++;
		}
	}
	return (tmp);
}
