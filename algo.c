/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/12 14:12:35 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/15 15:43:54 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		on_board(t_fill *f, int y, int x)
{
	if (x < 0 || y < 0 || x >= f->board_width || y >= f->board_height)
		return (0);
	else if (f->board[y][x] == f->p2 || f->board[y][x] == f->p2 + 32)
		return (0);
	return (1);
}

int		place_piece(t_fill *f)
{
	f->touch = 0;
	f->pi = -1;
	while (++f->pi < f->piece_height)
	{
		f->pj = -1;
		while (++f->pj < f->piece_width)
		{
			if (f->piece[f->pi][f->pj] == '*'
					&& on_board(f, f->py + f->pi, f->px + f->pj)
					&& f->board[f->py + f->pi][f->px + f->pj] != '.')
			{
				if (f->board[f->py + f->pi][f->px + f->pj] == f->p1 ||
						f->board[f->py + f->pi][f->px + f->pj] == f->p1 + 32)
					f->touch++;
				else
					return (0);
			}
			else if (f->piece[f->pi][f->pj] == '*'
					&& !on_board(f, f->py + f->pi, f->px + f->pj))
				return (0);
		}
	}
	if (f->touch == 1)
		return (1);
	return (0);
}

int		scan_piece(t_fill *f)
{
	int	i;
	int	j;

	i = f->piece_start_i;
	while (i != f->piece_end_i)
	{
		j = f->piece_start_j;
		while (j != f->piece_end_j)
		{
			if (f->piece[i][j] == '*')
			{
				f->px = f->bj - j;
				f->py = f->bi - i;
				if (place_piece(f))
					return (1);
			}
			j += f->piece_iter_j;
		}
		i += f->piece_iter_i;
	}
	return (0);
}

void	scan_board(t_fill *f)
{
	f->bi = f->board_start_i;
	while (f->bi != f->board_end_i)
	{
		f->bj = f->board_start_j;
		while (f->bj != f->board_end_j)
		{
			if (f->board[f->bi][f->bj] == f->p1
					|| f->board[f->bi][f->bj] == f->p1 + 32)
				if (scan_piece(f))
				{
					ft_printf("%d %d\n", f->py, f->px);
					return ;
				}
			f->bj += f->board_iter_j;
		}
		f->bi += f->board_iter_i;
	}
	ft_printf("0 0\n");
}
