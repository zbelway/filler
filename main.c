/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/06 21:45:47 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/15 15:41:45 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void		print_result(t_fill *f)
{
	if (f->p1_placed > 0)
		ft_printf("%!%{M}WE WON!!! :D %d more filled", 2, f->p1_placed);
	else if (f->p1_placed < 0)
		ft_printf("%!%{B}we lost %d less filled :(", 2, abs_val(f->p1_placed));
	else
		ft_printf("%!%{Y}TIE?!?! rematch?", 2);
	ft_printf("%!\n%{*C}", 2);
}

void		print_board(t_fill *f)
{
	f->bi = -1;
	f->p1_placed = 0;
	while (++f->bi < f->board_height)
	{
		f->bj = -1;
		while (++f->bj < f->board_width)
		{
			if (f->board[f->bi][f->bj] == '.')
				ft_printf("%!%{Y}.", 2);
			else if (f->board[f->bi][f->bj] == f->p1
					|| f->board[f->bi][f->bj] == f->p1 + 32)
			{
				ft_printf("%!%{R}O", 2);
				f->p1_placed++;
			}
			else if (f->board[f->bi][f->bj] == f->p2
					|| f->board[f->bi][f->bj] == f->p2 + 32)
			{
				ft_printf("%!%{G}X", 2);
				f->p1_placed--;
			}
		}
		ft_putchar_fd('\n', 2);
	}
	print_result(f);
}

int			line_start(char *s1, char *s2)
{
	int		i;

	i = 0;
	while (s1[i] && s2[i])
	{
		if (s1[i] == s2[i])
			i++;
		else
			return (0);
	}
	if (s2[i])
		return (0);
	return (1);
}

int			main(void)
{
	t_fill	f;
	char	*s;

	f.p1 = 0;
	f.p2 = 0;
	while (get_next_line(0, &s) > 0)
	{
		if (line_start(s, "$$$"))
			get_player(s, &f);
		if (line_start(s, "Plateau"))
			get_board(&s, &f);
		if (line_start(s, "Piece"))
		{
			get_piece(&s, &f);
			get_directionality(&f);
			scan_board(&f);
		}
	}
	print_board(&f);
	return (0);
}
