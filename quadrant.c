/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quadrant.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zbelway <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/14 15:59:34 by zbelway           #+#    #+#             */
/*   Updated: 2016/04/14 23:07:27 by zbelway          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int			closest_distance(t_fill *f, double *d)
{
	double	d1;
	double	d2;
	int		i;

	i = 0;
	d2 = distance(f->board_height, f->board_width);
	while (i < f->p2_placed)
	{
		d1 = distance(f->bi - f->p2y[i], f->bj - f->p2x[i]);
		if (d1 < d2)
			d2 = d1;
		i++;
	}
	if (d2 < *d)
	{
		*d = d2;
		return (1);
	}
	return (0);
}

int			start_direction(t_fill *f)
{
	double	d;

	f->pi = -1;
	d = distance(f->board_height, f->board_width);
	while (++f->pi < f->board_height)
	{
		f->pj = -1;
		while (++f->pj < f->board_width)
		{
			if (f->board[f->pi][f->pj] == f->p2
				|| f->board[f->pi][f->pj] == f->p2 + 32)
				if (closest_distance(f, &d))
				{
					f->px = f->pj;
					f->py = f->pi;
				}
		}
	}
	if (f->px <= f->bj && f->py <= f->bi)
		return (1);
	else if (f->px > f->bj && f->py <= f->bi)
		return (2);
	else if (f->px > f->bj && f->py > f->bi)
		return (3);
	return (4);
}

void		find_corner(t_fill *f, int q)
{
	change_side(f, q);
	f->bi = f->board_start_i;
	while (f->bi != f->board_end_i)
	{
		f->bj = f->board_start_j;
		while (f->bj != f->board_end_j)
		{
			if (f->board[f->bi][f->bj] == f->p1
					|| f->board[f->bi][f->bj] == f->p1 + 32)
				return ;
			f->bj += f->board_iter_j;
		}
		f->bi += f->board_iter_i;
	}
}

int			get_quadrant(t_fill *f, int placed)
{
	double	d;
	int		quadrant;

	d = distance(f->board_height, f->board_width);
	if (placed)
		find_corner(f, 1);
	if (placed == 1 || placed == 0)
		return (start_direction(f));
	if (closest_distance(f, &d))
		quadrant = 1;
	find_corner(f, 2);
	if (closest_distance(f, &d))
		quadrant = 2;
	find_corner(f, 3);
	if (closest_distance(f, &d))
		quadrant = 3;
	find_corner(f, 4);
	if (closest_distance(f, &d))
		quadrant = 4;
	return (quadrant);
}
